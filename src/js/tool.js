"use strict";

let radioGroups = document.getElementsByClassName("radio");
let body = document.body;

export function selectRadio() {
    for (let i = 0; i < radioGroups.length; i++) {
        let radioElements = radioGroups[i].children;
        for (let j = 0; j < radioElements.length; j++) {
            radioElements[j].addEventListener("click", () => {
                for (let k = 0; k < radioElements.length; k++) {
                    radioElements[k].classList.remove("selected");
                }
                radioElements[j].classList.add("selected");
            });
        }
    }
}

/**
 * Create HTML element with possibly content, id, class and children 
 *
 * @param string type
 * @param string content
 * @param string id
 * @param string CSSClass
 * @param array children
 */
export function createElt(type = "p", content = null, id = null, CSSClass = null, children = null) {
    let elt = document.createElement(type);
    if (content !== null) elt.textContent = content;
    if (id !== null) elt.setAttribute("id", id);
    if (CSSClass !== null) elt.className = CSSClass;
    if (children !== null) {
        children.forEach(element => {
            elt.appendChild(element)
        });
    }

    return elt;
}

export function getRandomInt(max, min = 0) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

export async function asyncCall(url) {
    let response = await fetch(url);
    let data = await response.json();

    return data;
}

export function createArray(trHead, tbody, id = null) {
    return createElt("table", null, id , null, [
        createElt("thead", null, null, null, [
            trHead
        ]),
        tbody
    ])
}

/**
 * Builds, displays then removes an informational pop-up
 *
 * @param string text
 * @param string skin
 */
export function displayPopup(text, skin = "dark") {
    let popupSection = document.getElementById('popup-section');
    let div = document.createElement('div');
    let p = document.createElement('p');
    let cross = document.createElement('div');
    let i = document.createElement('i');
    i.className = "fas fa-times";
    div.classList.add(skin);
    cross.classList.add("cross");
    p.innerHTML = text;
    cross.appendChild(i);
    div.appendChild(cross);
    div.appendChild(p);
    popupSection.appendChild(div);

    cross.addEventListener("click", () => {
        div.remove();
    });

    setTimeout(function () {
        div.remove();
    }, 10000);
}