"use strict";

import { Series } from "./class/series.js";

let startBtn = document.getElementById("start");

let max = 9;

let startSeries = document.getElementById("start-series");
let endSeries = document.getElementById("end-series");

startSeries.setAttribute("max", max - 1);
endSeries.setAttribute("max", max);

startBtn.addEventListener("click", () => {
    let series = new Series(startSeries.value, endSeries.value);
    series.init();
});

let next = JSON.parse(localStorage.getItem("next"));

if (next !== null) {
    let series = new Series(next.begin, next.end, next.deck, 0, 0);
    series.displaySeriesSection();
    series.drawCurrentCard();
    localStorage.removeItem("next");
}